<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Krinkle\Intuition\Intuition;
use League\CommonMark\CommonMarkConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
// phpcs:ignore MediaWiki.Classes.UnusedUseStatement.UnusedUse
use Symfony\Component\Routing\Annotation\Route;

class DocsController extends AbstractController {

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/", name="home")
	 * @param Connection $connection
	 * @param Intution $intuition
	 * @return Response
	 */
	public function home( Connection $connection, Intuition $intuition ): Response {
		$sql = 'SELECT t.name, t.title, t.description, ed.url, d.lang, d.version
			FROM tools t
				LEFT JOIN docs d ON ( t.id = d.tool_id )
				LEFT JOIN external_docs ed ON ( t.id = ed.tool_id )
			WHERE ed.lang = :lang OR d.lang = :lang
			GROUP BY t.id
			ORDER BY t.title ASC';
		$tools = $connection
			->executeQuery( $sql, [ 'lang' => $intuition->getLang() ] )
			->fetchAllAssociative();
		$langsSql = 'SELECT lang FROM external_docs GROUP BY lang UNION SELECT lang FROM docs GROUP BY lang';
		$langs = $connection->executeQuery( $langsSql )->fetchFirstColumn();
		return $this->render( 'home.html.twig', [
			'langs' => $langs,
			'tools' => $tools,
		] );
	}

	/**
	 * phpcs:ignore MediaWiki.Commenting.FunctionAnnotations.UnrecognizedAnnotation
	 * @Route("/{tool}/{lang}/{version}/{path}", name="page", requirements={"tool"="^(?!_)[^/]+", "path"=".+"})
	 * @param Connection $connection
	 * @param string $tool
	 * @param string $lang
	 * @param string $version
	 * @param string $path
	 * @return Response
	 */
	public function page(
		Connection $connection, string $tool, string $lang = 'en', string $version = 'latest', string $path = 'index'
	): Response {
		$page = $connection->executeQuery( '
		SELECT * FROM pages p
			JOIN docs d ON ( p.doc_id = d.id )
			JOIN tools t ON ( d.tool_id = t.id )
		WHERE path=:path AND version=:version AND lang=:lang AND t.name=:tool
		LIMIT 1
		', [
			'path' => "/$path",
			'version' => $version,
			'lang' => $lang,
			'tool' => $tool,
		] )->fetchAssociative();

		if ( !$page ) {
			throw $this->createNotFoundException( 'No documentation page found.' );
		}

		$converter = new CommonMarkConverter( [
			'html_input' => 'strip',
			'allow_unsafe_links' => false,
		] );

		return $this->render( 'page.html.twig', [
			'page' => $page,
			'html' => $converter->convertToHtml( $page['contents'] ),
		] );
	}
}
