<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class ImportCommand extends Command {

	/** @var string */
	protected static $defaultName = 'app:import';

	/** @var string */
	protected static $defaultDescription = 'Import docs from all repos in var/repos/';

	/** @var Connection */
	private $connection;

	/**
	 * @param Connection $connection
	 * @param string $projectDir
	 */
	public function __construct( Connection $connection, string $projectDir ) {
		parent::__construct();
		$this->connection = $connection;
		$this->projectDir = $projectDir;
	}

	protected function configure(): void {
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute( InputInterface $input, OutputInterface $output ): int {
		$finder = new Finder();
		$markdownFiles = $finder
			->files()
			->in( "$this->projectDir/var/repos" )
			->path( 'docs' )
			->name( '*.md' );
		foreach ( $markdownFiles as $markdownFile ) {
			$this->saveFile( $markdownFile );
		}
		return Command::SUCCESS;
	}

	/**
	 * Save a single markdown file's info and contents to the database.
	 * @param SplFileInfo $file
	 */
	private function saveFile( SplFileInfo $file ) {
		$pathParts = explode( DIRECTORY_SEPARATOR, $file->getRelativePath() );
		$pathParts[] = $file->getFilenameWithoutExtension();
		if ( count( $pathParts ) < 4 ) {
			// No toolname, 'docs', lang, and filename.
			return;
		}
		$name = $pathParts[0];
		$lang = $pathParts[2];
		unset( $pathParts[0], $pathParts[1], $pathParts[2] );
		$path = '/' . implode( '/', $pathParts );
		// @TODO Look up Git branch name (not here though).
		$version = 'latest';

		// Save tool if it's not already there.
		if ( !isset( $this->toolIds[$name] ) ) {
			$insertTool = 'INSERT IGNORE INTO tools SET name=:name, title=:name';
			$this->connection->executeStatement( $insertTool, [ 'name' => $name ] );
			$selectTool = 'SELECT id FROM tools WHERE name=:name LIMIT 1';
			$toolId = $this->connection->executeQuery( $selectTool, [ 'name' => $name ] )->fetchOne();
			$this->toolIds[$name] = $toolId;
		}
		$toolId = $this->toolIds[$name];

		// Save tool lang and version.
		$docIdKey = $toolId . $lang . $version;
		if ( !isset( $this->docIds[$docIdKey] ) ) {
			$params = [ 'tool_id' => $toolId, 'lang' => $lang, 'version' => $version ];
			$insertDocs = 'INSERT IGNORE INTO `docs` SET `tool_id`=:tool_id, `lang`=:lang, `version`=:version';
			$this->connection->executeStatement( $insertDocs, $params );
			$docIdSelect = 'SELECT id FROM `docs` WHERE `tool_id`=:tool_id AND `lang`=:lang AND `version`=:version';
			$docId = $this->connection->executeQuery( $docIdSelect, $params )->fetchOne();
			$this->docIds[$docIdKey] = $docId;
		}
		$docId = $this->docIds[$docIdKey];

		// Save page.
		$pagesSql = 'INSERT INTO pages SET doc_id=:doc_id, path=:path, contents=:contents'
			. ' ON DUPLICATE KEY UPDATE contents=:contents';
		$this->connection->executeStatement(
			$pagesSql,
			[ 'doc_id' => $docId, 'path' => $path, 'contents' => $file->getContents() ]
		);
	}
}
