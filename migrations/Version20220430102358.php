<?php

declare( strict_types=1 );

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220430102358 extends AbstractMigration {

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Initial install.';
	}

	/**
	 * @param Schema $schema
	 */
	public function up( Schema $schema ): void {
		$this->addSql(
			'CREATE TABLE IF NOT EXISTS `tools` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`name` VARCHAR(190) NOT NULL,
				`title` VARCHAR(500) NOT NULL,
				`description` TEXT NOT NULL,
				UNIQUE INDEX UNIQ_TOOLS_NAME (`name`)
			) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;'
		);
		$this->addSql(
			'CREATE TABLE IF NOT EXISTS `external_docs` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				`tool_id` INT(10) UNSIGNED NOT NULL,
				`lang` VARCHAR(10) NOT NULL,
				`url` VARCHAR(500) NOT NULL,
				FOREIGN KEY (`tool_id`) REFERENCES `tools` (`id`) ON DELETE CASCADE,
				UNIQUE INDEX UNIQ_EXTDOCS_TOOL_LANG (`tool_id`, `lang`)
			) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;'
		);
	}

	/**
	 * @param Schema $schema
	 */
	public function down( Schema $schema ): void {
		$this->addSql( 'DROP TABLE IF EXISTS external_docs' );
		$this->addSql( 'DROP TABLE IF EXISTS tools' );
	}
}
