---
title: ToolDocs Docs
---
The **Tools' Documentation Viewer** (or **ToolDocs** for short) is a Wikimedia Toolforge tool for viewing the user-documentation of any tool.
It provides an index to all available documentation,
a means to have separate documentation for different tool versions and languages,
and an interface for translating documentation.

It aims to be roughly similar to the popular [Read the Docs](https://readthedocs.org) service,
but with Wikimedia-specific functionality.

ToolDocs links to documentation hosted outside its own system for some tools,
but its main utility is when documentation is written within a tool's Git repository.
This is known as the ['docs-as-code'](https://www.writethedocs.org/guide/docs-as-code/) approach
(or 'docs-in-code'), and is a popular system of managing documentation.
It involves storing documentation text files (usually [Markdown](https://www.markdownguide.org/getting-started/))
within the same repository as the main software code,
and keeping this documentation up to date as the code changes.
The documentation files are then compiled to HTML and make available on the web (this is where ToolDocs comes in).

The primary advantages of this approach are that the documentation only ever needs to address the current state of the code
(i.e. doesn't repeatedly have to call out differences of behaviour between versions),
and continuous-integration processes can be put in place to help ensure that
every code change is either accompanied by a documentation change or a declaration that no such change is required.
Version- and language-switching dropdowns are available on every page of the documentation.

Common tools for docs-in-code are [https://www.sphinx-doc.org/ Sphinx] and [https://www.mkdocs.org/ MkDocs].
The former is the basis for the popular [https://readthedocs.org/ Read the Docs] website,
and the latter is used for the new Wikimedia [Developer Portal](https://www.mediawiki.org/wiki/Developer_Advocacy/Developer_Portal).

## Adding a project

As no documentation is actually stored within ToolDocs,
it is necessary to first create a Git repository in which to store the documentation source.
This is usually the same repository as a tool's software source code,
and we recommend using [Wikimedia GitLab](https://gitlab.wikimedia.org).

ToolDocs will attempt to autodiscover your repository and documentation via Toolhub,
so if your tool is registered there, there should be no further action required.

If you want to add a project manually, first log in via the main menu
(you will be redirected to Meta Wiki to authorize ToolDocs, and then back again),
then enter your project's Git URL in the "New project:" form field on the homepage.
The HTTPS URL should be used (rather than an SSH one), for example: `https://gitlab.wikimedia.org/exampleuser/example-project.git`.

This will add your project to the system, and give you access to an admin area in which you can change your project's settings.
If your tool has already been added automatically from Toolhub, you will have access to this admin area
if your username on ToolDocs matches one listed as an author of the tool on Toolhub.

After the project has been added,
ToolDocs will clone your repository and build the documenation,
making it available at a URL such as `https://docs.toolforge.org/example-project/`.
This process runs every ten minutes, and will also check for correct structure and configuration of the files (see below).

## Documentation structure

Documentation files should be in a `docs/` directory in your project repository,
and within that grouped by language code.

The structure within each language directory is completely up to you.
The only requirement is that there is an `index.md` file in each directory.
The file names and directory structure will be used in the URLs for the published documentation pages.

For general information about writing documentation,
the [Write the Docs](https://www.writethedocs.org/) website is a useful resource.
